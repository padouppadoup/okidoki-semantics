Okidoki Semantic
================

Upstream documentation. Annotated.


# How to add a documentation
```
git subtree add --prefix sources/docs.nextcloud.com https://github.com/nextcloud/documentation.git master
```

# Existing tags
```
- admin
- beginner
- advanced
- plugin:<name of plugin>
```
